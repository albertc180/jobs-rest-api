exports = module.exports = GetHandler;

const connect = require('../connect/connect'),
    functions = require('./functions'),
    path = require('path');

function GetHandler() {
    if (!(this instanceof GetHandler)) return new GetHandler();

}


GetHandler.prototype.init = function (app, express) {

    const pool = connect.getDBpool();


    // USER ENTITIES

    app.get("/users", (req, res) => {

        pool.getConnection(function (connectionErr, connection) {
            if (!connectionErr) {

                // getting single user record
                connection.query('SELECT * FROM `users` ', function (error, userResults, fields) {
                    if (error) {
                        res.status(500).send(error);
                    } else {
                        // creating new object to avoid exposing user password has directly in response

                        let userObjects = [];
                        userResults.forEach((userObj, index) => {
                            userObjects.push({
                                login: userObj.login,
                                create_date: userObj.create_date
                            })
                        });
                        res.send(userObjects);
                        connection.release();
                    }
                });
            } else {
                res.status(500).send(connectionErr);
            }
        });
    });


    // JOBS ENTITIES


    // getting all jobs from app database
    app.get("/jobs", (req, res) => {
        let jobQuery;
        // iterating over params object (just in case)
        // for (var param in req.query) {
        //     if (req.query.hasOwnProperty(param)) {
        //     }
        // }

        // handling get parameteres (filter search)
        if (req.query.hasOwnProperty("category") &&
            !req.query.hasOwnProperty("offering_company")) {
            jobQuery = 'SELECT * FROM `job_offers` WHERE `category` = "' + req.query["category"] + '"';
        } else if (req.query.hasOwnProperty("offering_company") &&
            !req.query.hasOwnProperty("category")) {
            jobQuery = 'SELECT * FROM `job_offers` WHERE `offering_company` = "' + req.query["offering_company"] + '"';
        } else if (req.query.hasOwnProperty("offering_company") &&
            req.query.hasOwnProperty("category")) {
            jobQuery = 'SELECT * FROM `job_offers` WHERE `category` = "' + req.query["category"] + '"' + ' AND `offering_company` = "' + req.query["offering_company"] + '"';
        } else {
            jobQuery = 'SELECT * FROM `job_offers`';
        }
        pool.getConnection(function (connectionErr, connection) {
            if (!connectionErr) {
                connection.query(jobQuery, function (error, results, fields) {
                    if (error) {
                        res.status(500).send(error);
                    } else {
                        let jobsObj = results;
                        jobsObj.forEach((jobObj, index) => {
                            functions.getCategory(jobsObj[index].category).then(catResult => {
                                    if (catResult.queryStatus) {
                                        jobObj.category = catResult.catTitle;
                                    } else {
                                        jobObj.category = null;
                                    }
                                })
                                .catch(err => {
                                    res.status(500).send(err);
                                });
                            functions.getUser(jobsObj[index].added_by).then(userResult => {
                                    if (userResult.queryStatus) {
                                        jobObj.added_by = userResult.username;
                                    } else {
                                        jobObj.added_by = null;
                                    }
                                    if (index === jobsObj.length - 1) {
                                        res.send(jobsObj);
                                    }
                                })
                                .catch(err => {
                                    console.log(err);
                                    res.status(500).send(err);
                                });
                        });
                    }
                });
            } else {
                res.status(500).send(connectionErr);
            }
        });
    });




    // OTHER REQUESTS

    // dummy page - for quick check if express server is operating normally
    // TODO - remove or replace on production
    app.get('/', (req, res) => {
        res.sendFile(path.join(__dirname + '/view/index.html'));

    });

}