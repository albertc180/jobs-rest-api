const connect = require('../connect/connect'),
    pool = connect.getDBpool();

module.exports = {
    getCategory: function (catID) {
        return new Promise((resolve, reject) => {
            pool.getConnection(function (connectionErr, connection) {
                if (!connectionErr) {
                    connection.query('SELECT * FROM `job_categories` WHERE ID = ?', [catID], function (error, results, fields) {
                        if (error) {
                            return reject(error)
                        } else {
                            // creating object to pass status and record ID in promise callback
                            let catObject = {};
                            if (results.length !== 0) {
                                catObject.queryStatus = true;
                                catObject.catTitle = results[0].title;
                                connection.release();
                                return resolve(catObject)
                            } else {
                                catObject.queryStatus = false;
                                connection.release();
                                return resolve(catObject)
                            }
                        }
                    });
                } else {
                    return reject(connectionErr)
                }
            });
        });
    },
    getUser: function (userID) {
        return new Promise((resolve, reject) => {
            pool.getConnection(function (connectionErr, connection) {
                if (!connectionErr) {
                    connection.query('SELECT login FROM `users` WHERE ID = ?', [userID], function (error, results, fields) {
                        if (error) {
                            return reject(error)
                        } else {
                            // creating object to pass status and record ID in promise callback
                            let userObject = {};
                            if (results.length !== 0) {
                                userObject.queryStatus = true;
                                userObject.username = results[0].login;
                                connection.release();
                                return resolve(userObject)
                            } else {
                                userObject.queryStatus = false;
                                connection.release();
                                return resolve(userObject)
                            }
                        }
                    });
                } else {
                    return reject(connectionErr)
                }
            });
        });
    }
};