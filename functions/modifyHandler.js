exports = module.exports = ModifyHandler;

const connect = require('../connect/connect'),
    md5 = require('md5'),
    authenticate = require('../connect/authenticate');

function ModifyHandler() {
    if (!(this instanceof ModifyHandler)) return new ModifyHandler();

}


ModifyHandler.prototype.init = function (app, express) {

    const pool = connect.getDBpool();

    // USER ENTITIES


    // adding user
    app.post('/users', (req, res) => {
        let postParams = req.body;
        if (postParams.hasOwnProperty('login') &&
            postParams.hasOwnProperty('pass')) {


            pool.getConnection(function (connectionErr, connection) {
                if (!connectionErr) {
                    connection.query('INSERT INTO `users` SET login = ?, pass= ?, create_date= ?',
                        [postParams.login,
                            // TODO replace md5 with safer solution
                            // md5 is not the best solution :) needs to be changed to SHA or similar
                            // but at this time this solution is enough
                            md5(postParams.pass),
                            new Date(),
                        ],
                        function (error, results, fields) {
                            if (error) {
                                res.status(422).send(error);
                            } else {
                                res.status(201).send({
                                    login: postParams.login,
                                    id: results.insertId,
                                    status: "insert successfull"
                                });
                            }
                        });
                }
            });
        } else {
            res.status(400).send('Invalid POST parameters');
        }
    });

    // updating user
    app.put('/users', (req, res) => {
        let postParams = req.body;
        //login and pass for authentication
        if (postParams.hasOwnProperty('login') &&
            postParams.hasOwnProperty('pass')) {
            // authenticate user
            authenticate.passAuth(postParams.login, postParams.pass).then(authResult => {
                    if (authResult.authStatus) {
                        let newLogin;
                        let newPass;
                        // new login and (or) pass to update
                        if (postParams.hasOwnProperty('newlogin') ||
                            postParams.hasOwnProperty('newpass')) {
                            // TODO fix if statements mess
                            // lets face it - its really lame solution ;) need to refactor it to something more elegant
                            if (postParams.hasOwnProperty('newlogin')) {
                                newLogin = postParams.newlogin;
                            } else {
                                newLogin = postParams.login;
                            }
                            if (postParams.hasOwnProperty('newpass')) {
                                newPass = postParams.newpass;
                            } else {
                                newPass = postParams.pass;
                            }

                            pool.getConnection(function (connectionErr, connection) {
                                if (!connectionErr) {
                                    connection.query('UPDATE `users` SET login = ?, pass= ? WHERE ID = ?',
                                        [newLogin,
                                            md5(newPass),
                                            authResult.recordID
                                        ],
                                        function (error, results, fields) {
                                            if (error) {
                                                res.status(422).send(error);
                                            } else {
                                                res.send({
                                                    login: postParams.login,
                                                    newLogin: newLogin,
                                                    id: results.insertId,
                                                    status: "update successfull"
                                                });
                                            }
                                        });
                                }
                            });
                        } else {
                            res.status(400).send('Invalid POST parameters');
                        }

                    } else {
                        res.status(401).send("authentication error");
                    }
                })
                .catch(err => {
                    res.status(422).send(err);
                });
        } else {
            res.status(400).send('Invalid POST parameters');
        }
    });

    // deleting user
    app.delete('/user', (req, res) => {
        let postParams = req.body;
        if (postParams.hasOwnProperty('login') &&
            postParams.hasOwnProperty('pass')) {
            // authenticate user
            authenticate.passAuth(postParams.login, postParams.pass).then(authResult => {
                    if (authResult.authStatus) {

                        pool.getConnection(function (connectionErr, connection) {
                            if (!connectionErr) {
                                connection.query('DELETE FROM `users` WHERE ID = ?',
                                    [authResult.recordID],
                                    function (error, results, fields) {
                                        if (error) {
                                            res.status(422).send(error);
                                        } else {
                                            res.status(204).send({
                                                login: postParams.login,
                                                status: "delete successfull"
                                            });
                                        }
                                    });
                            }
                        });
                    } else {
                        res.status(401).send("authentication error");
                    }
                })
                .catch(err => {
                    res.status(422).send(err);
                });


        } else {
            res.status(400).send('Invalid POST parameters');
        }
    });




    // JOBS ENTITIES


    app.post('/jobs', (req, res) => {
        let postParams = req.body;
        // TODO refactor multiple if statement
        if (postParams.hasOwnProperty('login') &&
            postParams.hasOwnProperty('pass') &&
            postParams.hasOwnProperty('title') &&
            postParams.hasOwnProperty('category') &&
            postParams.hasOwnProperty('date_start') &&
            postParams.hasOwnProperty('date_end') &&
            postParams.hasOwnProperty('offering_company')) {
            // authenticate user
            authenticate.passAuth(postParams.login, postParams.pass).then(authResult => {
                    if (authResult.authStatus) {

                        pool.getConnection(function (connectionErr, connection) {
                            if (!connectionErr) {
                                connection.query('INSERT INTO `job_offers` SET title = ?, category= ?, date_start = ?, date_end = ?, offering_company = ?, added_by = ?',
                                    [postParams.title,
                                        postParams.category,
                                        postParams.date_start,
                                        postParams.date_end,
                                        postParams.offering_company,
                                        authResult.recordID
                                    ],
                                    function (error, results, fields) {
                                        if (error) {
                                            res.status(422).send(error);
                                        } else {
                                            connection.release();
                                            res.status(201).send({
                                                jobTitle: postParams.title,
                                                id: results.insertId,
                                                status: "insert successfull"
                                            });
                                        }
                                    });
                            }
                        });
                    } else {
                        res.status(401).send("authentication error");
                    }
                })
                .catch(err => {
                    res.status(422).send(err);
                });
        } else {
            res.status(400).send('Invalid POST parameters');
        }
    });

}