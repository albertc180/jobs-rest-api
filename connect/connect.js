const mysql = require("mysql");

var pool;

    module.exports = {
        getDBpool: function () {
          if (pool) return pool;
          pool = mysql.createPool({
            host: DBHOST,
            user: DBUSER,
            password: DBPASS,
            database: DB,
            dateStrings: true
            // charset: "utf8mb4"
        });
          return pool;
        }
    };

