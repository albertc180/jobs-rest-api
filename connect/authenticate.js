const connect = require('./connect'),
    md5 = require('md5'),
    pool = connect.getDBpool();

module.exports = {
    // TODO - multiple permission level access
    // right now only user itself can modify his record - there should be some admin privilege that is able to modify any record
    passAuth: function (user, pass) {
        return new Promise((resolve, reject) => {

            let hashPass = md5(pass);

            pool.getConnection(function (connectionErr, connection) {
                if (!connectionErr) {
                    connection.query('SELECT * FROM users WHERE login = ?', [user], function (error, results, fields) {
                        if (error) {
                            return reject(error)
                        } else {
                            // creating object to pass status and record ID in promise callback
                            let authObject = {};
                            if (results.length !== 0 &&
                                hashPass === results[0].pass) {
                                authObject.authStatus = true;
                                authObject.recordID = results[0].ID;
                                connection.release();
                                return resolve(authObject)
                            } else {
                                authObject.authStatus = false;
                                connection.release();
                                return resolve(authObject)
                            }
                        }
                    });
                } else {
                    return reject(connectionErr)
                }
            });
        });
    }
};