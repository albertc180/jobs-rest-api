const express = require('express'),
    bodyParser = require('body-parser'),
    request = require('request'),
    path = require('path');


// external config (db credentials etc)
const appConfig = require('./config/appConfig')(),
    // request handling functions
    GetHandler = require("./functions/getHandler"),
    getHandler = new GetHandler(),
    ModifyHandler = require("./functions/ModifyHandler"),
    modifyHandler = new ModifyHandler(),
    // setting port for express
    PORT = process.env.PORT || EXPRESSPORT;

var app = express();


// express routing

app.use('/', express.static(path.join(__dirname, 'view')));

// setting bodyparser for parsing requests
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));


// starting server
app.listen(PORT, function () {
    console.log('Listening on: ' + PORT);
});

// handling requests

getHandler.init(app, express);
modifyHandler.init(app, express);