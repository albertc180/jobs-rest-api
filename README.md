# DNA_Entities

Simple REST API with jobs offer catalogue alike functionality. Offers few features:  
1. Getting all job offers from database  
2. Adding new job offer 
3. User authentication  
4. User management(CRUD)  
5. Fetching all comments from local database  


### ENVIRONMENT ###
API WAS TESTED ON:  
node.js 8.12.0  
npm 6.4.1  
mysql 5.5.61  



### INSTALL ###
1. Copy files to the destination directory.

2. Just run
```
$> npm install
```

3. Create MySQL database using SQL DB structure provided in this repository



### CONFIGURE ###
1. go to config --> appConfig.js and assign according values to the variables(DB credentials)
2. Change application port on config/appConfig.js:13 if there is need to



### RUN ###
1. Just use 
```
$> npm start
```




### USAGE ###
1. To fetch all users in DB(wihout their passwords) send GET request to {server_URL}/users
2. To fetch all jobs in DB send GET request to {server_URL}/jobs
3. To add new user send POST {server_URL}/users with 2 parameters:
- login (required)  
- pass (required)  
4. To modify existing user you need to send PUT request to {server_URL}/users with proper parameters - current login nad pass for authentication and new login (and/or) password. 
- login (required)  
- pass (required)  
- newlogin (optional)  
- newpass (optional)  
5. To delete existing user you need to send DELETE request to {server_URL}/users with proper parameters:
- login (required)  
- pass (required)  
6. To add new job offer you need to send POST request to {server_URL}/jobs with proper parameters:
- login (required)  
- pass (required)  
- title - job offer title  
- date_start - job offer starting date  
- date_end - job offer ending date  
- category - (number of category, currently 1-5)  
- offering_company - (name of company, in string)  


### TODO ###

1. Field selection
2. Pagination
3. Sorting
4. Searching
3. API versioning request

